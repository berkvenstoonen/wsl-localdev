# ssl
sudo mkdir '/usr/local/ssl'
echo # Blank Line
echo '########### Setting up Certificate Authority ###########'
echo 'It is important to remember the Certificate Authority passphrase as it is needed for every site you setup with SSL'
echo "We suggest the following answers if you're developing for BeTo:"
echo # Blank Line
echo 'Country Name:             NL'
echo 'State or Province Name:   Noord-Brabant'
echo 'Locality Name:            Leende'
echo 'Organization Name:        Stichting BeTo'
echo 'Organizational Unit Name: be-to.nl'
echo 'Common Name:              BeTo'
echo 'Email Address:            beto@localdev'
echo # Blank Line
echo 'You will now be asked for the Certificate Authority three times (enter, confirm, use)'
sudo openssl genrsa -des3 -out '/usr/local/ssl/CertificateAuthority.key' 2048
sudo openssl req -x509 -new -nodes -key '/usr/local/ssl/CertificateAuthority.key' -sha256 -days 1825 -out '/usr/local/ssl/CertificateAuthority.pem'

# wsl config
echo '[user]' | sudo tee '/etc/wsl.conf'
echo "default=${USER}" |sudo tee -a '/etc/wsl.conf'

sudo cp 'wsl-localdev/sudoers' '/etc/sudoers.d/wsl_localdev'
sudo cp 'wsl-localdev/init-wsl' '/etc/init-wsl'
sudo chmod +x '/etc/init-wsl'

# configure sshd
sudo ssh-keygen -A
sudo sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' '/etc/ssh/sshd_config'
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' '/etc/ssh/sshd_config'
sudo systemctl enable ssh
sudo service ssh start

# misc binaries
sudo apt-get update -y
sudo apt-get install -y default-jre zsh yamllint unzip

# elasticsearch TODO @JB replace with installing the newest version
wget -qO - 'https://artifacts.elastic.co/GPG-KEY-elasticsearch' | sudo apt-key add -
echo 'deb https://artifacts.elastic.co/packages/7.x/apt stable main' | sudo tee '/etc/apt/sources.list.d/elastic-7.x.list'
sudo apt-get update
sudo apt-get install elasticsearch -y
sudo systemctl enable elasticsearch
sudo service elasticsearch start

# redis
sudo add-apt-repository ppa:chris-lea/redis-server -y
sudo apt-get update
sudo apt-get install redis-server -y
sudo systemctl enable redis-server
sudo service redis-server start

# mysql
sudo apt-get install mysql-server -y
echo 'skip-log-bin' | sudo tee -a '/etc/mysql/mysql.conf.d/mysqld.cnf'
sudo service mysql start
sudo systemctl enable mysql
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root'" | sudo mysql

# php
sudo add-apt-repository ppa:ondrej/php -y
sudo apt update
sudo apt install -y php8.1 php8.1-curl php8.1-bcmath php8.1-gd php8.1-gmp php8.1-imagick php8.1-mbstring php8.1-mysqli php8.1-redis php8.1-soap php8.1-xml php8.1-zip php8.1-ldap php8.1-fpm php8.1-ast
sudo systemctl enable php8.1-fpm
sudo service php8.1-fpm start

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
echo "0 0 * * *  /usr/local/bin/composer self-update >/dev/null 2>&1" > tmp.cron
sudo crontab tmp.cron
rm tmp.cron

echo 'date.timezone = Europe/Amsterdam' | sudo tee -a /etc/php/8.1/fpm/php.ini
echo 'date.timezone = Europe/Amsterdam' | sudo tee -a /etc/php/8.1/cli/php.ini
echo 'phar.readonly = Off' | sudo tee -a /etc/php/8.1/cli/php.ini

# apache
sudo apt-get install apache2 -y
sudo a2enmod ssl proxy_fcgi setenvif expires headers http2 rewrite status
sudo a2enconf php8.1-fpm
sudo mkdir /etc/apache2/certificates
sudo mkdir /etc/apache2/certificates/private

sudo systemctl enable apache2
sudo service apache2 restart

# nodejs/npm/pnpm
curl -fsSL 'https://deb.nodesource.com/setup_lts.x' | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g pnpm

# sassc
sudo apt-get install -y sassc
sudo ln -s '/usr/bin/sassc' '/usr/local/bin/sassc-3.6.1'

# uglifyjs
sudo npm install uglify-es -g

# git preferences
git config --global push.default simple
git config --global core.autocrlf input

# Laravel
composer global require laravel/installer

# zsh
CHSH=no RUNZSH=no sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
cp 'wsl-localdev/.zshrc' "/home/${USER}/"
git clone 'https://github.com/TamCore/autoupdate-oh-my-zsh-plugins' "/home/${USER}/.oh-my-zsh/custom/plugins/autoupdate"
git clone 'https://github.com/zsh-users/zsh-syntax-highlighting.git' "/home/${USER}/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting"
git clone 'git@gitlab.com:berkvenstoonen/beto-zsh.git' "/home/${USER}/.oh-my-zsh/custom/plugins/beto"
echo 'Changing default shell'
chsh -s '/usr/bin/zsh'

# cleanup
rm -rf 'wsl-localdev'
echo 'All done. Relogin to activate zsh shell'
