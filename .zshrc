export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="steeef"
plugins=(
    git
    composer
    ssh-agent
    zsh-syntax-highlighting
    autoupdate
    beto
)
source $ZSH/oh-my-zsh.sh
export UPDATE_ZSH_DAYS=1
