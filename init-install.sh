#!/bin/bash

echo # Blank Line
echo '########### Generating an SSH key for GitLab ###########'
echo # Blank Line
ssh-keygen -t ed25519 -f "${HOME}/.ssh/id_ed25519"
echo '-------------------- Your newly generated public key --------------------'
cat "${HOME}/.ssh/id_ed25519.pub"
echo '-------------------------------------------------------------------------'
read -p 'Copy and register above public key first @ https://gitlab.com/-/profile/keys before continuing. Done and ready to continue? [Yn] ' -n 1 -r
echo # Blank Line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

git clone 'git@gitlab.com:berkvenstoonen/wsl-localdev.git'
bash 'wsl-localdev/install.sh'
