# Install

1. Make sure that CPU virtualization is enabled in the BIOS

2. Enable automatic kernel updates:  
Windows 10: Update & security -> Advanced options -> Receive updates for other Microsoft products when you update Windows
Windows 11: Windows update > Update & security > Receive updates for other Microsoft products when you update Windows

3. Start powershell as administrator and install Ubuntu:
```
wsl --install -d Ubuntu
```

When running this the fist time, the machine will need to reboot.

4. Login on your new environment and start the installation:
```
bash -c "$(curl -fsSL https://gitlab.com/berkvenstoonen/wsl-localdev-setup/-/raw/main/init-install.sh)"
```
Note that the installer will ask you multiple times for your private key passphrase, your sudo password and shell password. Read carefully which password needs to be provided for the installer will most likely crash when provided with an incorrect password, needing a full re-install.

5. Start powershell as administrator and run this command to finish the setup:
```
iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/berkvenstoonen/wsl-localdev-setup/-/raw/main/finish-install.bin'))
```

## Post install

These settings still need to be configured after the installation:

1. Configure GIT
```
git config --global user.email "username@email.com"
git config --global user.name "Full Name"
```

# Reinstall
1. Start powershell as administrator
```
wsl --unregister Ubuntu
```

2. Repeat Install steps 3 and 4
